from flask import Flask,request,jsonify
from sqllib import general_dashboard_data_potensi, general_dashboard_data_scrap, general_dashboard_data_scrap_kt_primer, general_dashboard_data_scrap_kttl, general_dashboard_data_scrap_other, general_dashboard_data_scrap_sekunder, general_dashboard_data_survey, general_dashboard_data_survey_kt_primer, general_dashboard_data_survey_kttl, general_dashboard_data_survey_other, general_dashboard_data_survey_sekunder, get_potensi_all_base_on_year, get_potensi_all_base_on_year_regional, get_potensi_all_base_on_year_regional_witel, get_witel_user_base_on_token, input_user,cek_user_exist,cek_otp_user,cek_login_user,cek_status_user_base_id_sandi
from sqllib import get_potensi_all,update_otp,update_token_base_id,get_perusahaan,update_last_login_base_on_token,compare_date,verified_token
from sqllib import get_order_scrap,get_order_scrap_detail,cek_order_scrap,update_order_scrap,get_otoritas_base_token,cek_id_potensi
from sqllib import input_hasil_scrap,get_all_hasil_scrap,get_detail_hasil_scrap,cek_id_scrap,update_hasil_scrap,input_potensi,update_potensi
from sqllib import get_witel_base_regional,get_sto_base_witel,get_lat_lng_base_sto,input_order,get_dashboard_teknik,search_data_from_dashboard_teknik
from sqllib import get_potensi_detail,get_list_all_sto,input_sto,update_sto,delete_sto,search_sto,cek_existed_sto
from sqllib import get_list_all_jenis_potensi,input_jenis_potensi,update_jenis_potensi,delete_jenis_potensi,search_jenis_potensi,cek_existed_jenis_potensi
from sqllib import get_data_user_name_id_base_on_token,change_username_and_pass
from sqllib import cek_user_pass_old_base_on_token,update_status_potensi,cek_existed_id_potensi_in_order_for_update_potensi
from sqllib import get_regional_for_regist,get_potensi_base_on_witel,get_dashboard_teknik_for_waspang,search_data_from_dashboard_teknik_for_waspang
from sqllib import get_otoritas_id_base_token,get_order_scrap_base_on_witel,get_actor_id_base_token
from sqllib import update_status_order_in_update_potensi,get_potensi_base_on_regional
from sqllib import get_regional_user_base_on_token,get_order_scrap_base_on_regional
from sqllib import get_dashboard_teknik_base_on_regional,search_data_from_dashboard_teknik_base_on_regional,get_year_that_existed_in_potensi
from sqllib import get_rekap_scrap_base_date_range,get_rekap_scrap_regional_and_date_range,get_rekap_scrap_regional_witel_and_date_range
from datetime import datetime
import hashlib
import json
from waitress import serve
app = Flask(__name__)
# CORS(app)

@app.route('/scrap/user/regist',methods=['POST'])
def user_input():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'user_id' not in json_data or 'nama' not in json_data or 'perusahaan' not in json_data or 'regional' not in json_data or 'witel' not in json_data or 'photo' not in json_data or 'sandi' not in json_data :
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            user_id = json_data['user_id']
            nama = json_data['nama']
            perusahaan = json_data['perusahaan']
            regional = json_data['regional']
            witel = json_data['witel']
            photo = json_data['photo']
            sandi = json_data['sandi']
            sandi = hashlib.sha256(sandi.encode()).hexdigest()
            cek_user = cek_user_exist(user_id)
            if cek_user==False:
                result = {"message":"user already existed"}
                resp = jsonify(result)
                return resp,208
            else:
                input_user(user_id,nama,perusahaan,regional,witel,photo,sandi)
                result = {"message":"regist success"}
                resp = jsonify(result)
                return resp,202
            
@app.route('/scrap/user/login',methods=['POST'])
def user_login():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'user_id' not in json_data or 'sandi' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            nama = json_data['user_id']
            sandi = json_data['sandi']
            sandi = hashlib.sha256(sandi.encode()).hexdigest()
            cek_user = cek_login_user(nama,sandi)
            if cek_user==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                cek_status = cek_status_user_base_id_sandi(nama)
                if cek_status==False:
                    result = {"message":"Unchecked User"}
                    resp = jsonify(result)
                    return resp,203
                else:
                    token = update_token_base_id(nama,sandi)
                    otoritas = get_otoritas_id_base_token(token)
                    result = {"message":token,
                              "otoritas":otoritas}
                    resp = jsonify(result)
                    return resp,200
                    
                
@app.route('/scrap/user/verify',methods=['POST'])
def cek_token():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek == False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                update_last_login_base_on_token(token)
                compare = compare_date(token)
                if compare==False:
                    result = {"message":"Token expired"}
                    resp = jsonify(result)
                    return resp,202
                else:
                    otoritas = get_otoritas_id_base_token(token)
                    result = {"message":"Success",
                              "otoritas":otoritas}
                    resp = jsonify(result)
                    return resp,200
                
# @app.route('/scrap/user/otp',methods=['POST'])
# def user_otp():
#     json_data = request.json
#     if json_data==None:
#         result = {"message":"process failed",
#                   "token":"-"}
#         resp = jsonify(result)
#         return resp,400
#     else:
#         if 'otp' not in json_data:
#             result = {"message":"error request",
#                       "token":"-"}
#             resp = jsonify(result)
#             return 401
#         else:
#             otp = json_data['otp']
#             cek_otp = cek_otp_user(otp)
#             if cek_otp==False:
#                 result = {"message":"Forbidden",
#                           "token":"-"}
#                 resp = jsonify(result)
#                 return resp,403
#             else:
#                 token = update_token_base_otp(otp)
#                 if token==None:
#                     result = {"message":"Non-auth info",
#                               "token":"-"}
#                     resp = jsonify(result)
#                     return resp,203
#                 else:
#                     result = {"message":"OTP verified",
#                               "token":token}
#                     resp = jsonify(result)
#                     return resp,200

@app.route('/scrap/get/order+scrap',methods=['POST'])
def show_order_scrap():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                otoritas = int(get_otoritas_id_base_token(token))
                if otoritas<3:
                    regional = get_regional_user_base_on_token(token)
                    if regional==0:
                        result = get_order_scrap()
                    else:
                        result = get_order_scrap_base_on_regional(regional)
                    return result,200
                else:
                    witel = get_witel_user_base_on_token(token)
                    regional = get_regional_user_base_on_token(token)
                    if regional==0:
                        result = get_order_scrap()
                        return result,200 
                    else:
                        if witel == "ALL":
                            result = get_order_scrap_base_on_regional(regional)
                        else:
                            result = get_order_scrap_base_on_witel(witel)
                        return result,200  
                
@app.route('/scrap/get/order+scrap+detail',methods=['POST'])
def show_detail_order_scrap():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'id_order' not in json_data or 'id_potensi' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            Id_order = json_data['id_order']
            Id_potensi = json_data['id_potensi']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                cek_order = cek_order_scrap(Id_order,Id_potensi)
                if cek_order==False:
                    result = []
                    resp = json.dumps(result)
                    return resp,204
                else:
                    result = get_order_scrap_detail(Id_order,Id_potensi)
                    return result,200
                
                
@app.route('/scrap/update/order+scrap',methods=['POST'])
def order_scrap_update():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'mitra' not in json_data or 'panjang' not in json_data or 'jumlah' not in json_data or 'satuan2' not in json_data or 'jumlah2' not in json_data or 'waspang_telkom' not in json_data or 'waspang_mitra' not in json_data or 'mitra_pelaksana' not in json_data or 'status_order' not in json_data or 'id_order' not in json_data or 'id_potensi' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            note = "-"
            if 'note' in json_data:
                note = json_data['note']
            mitra = json_data['mitra']
            panjang = json_data['panjang']
            jumlah = json_data['jumlah']
            satuan2 = json_data['satuan2']
            jumlah2 = json_data['jumlah2']
            waspang_telkom = json_data['waspang_telkom']
            waspang_mitra = json_data['waspang_mitra']
            mitra_pelaksana = json_data['mitra_pelaksana']
            status_order = json_data['status_order']
            id_order = json_data['id_order']
            id_potensi = json_data['id_potensi']
            token = json_data['token']
            token_cek = verified_token(token)
            if token_cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                cek_order = cek_order_scrap(id_order,id_potensi)
                if cek_order==False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    agen = get_actor_id_base_token(token)
                    update_order_scrap(mitra,panjang,jumlah,satuan2,jumlah2,note,waspang_telkom,waspang_mitra,mitra_pelaksana,status_order,agen,id_order,id_potensi)
                    update_status_potensi(id_potensi,agen,status_order)
                    result = {"message":"Update Success"}
                    resp = jsonify(result)
                    return resp,200
            
            
@app.route('/scrap/input/hasil+scrap',methods=['POST'])
def hasil_scrap_input():
    json_data = request.json
    if json_data == None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'id_order' not in json_data or 'jenis_barang' not in json_data or 'dt_scrap' not in json_data or 'nama_barang' not in json_data or 'from_' not in json_data or 'to_' not in json_data or 'pair_' not in json_data or 'diameter' not in json_data or 'potongan_4m' not in json_data or 'potongan_sisa' not in json_data or 'satuan' not in json_data or 'jumlah' not in json_data or 'note' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Id_order = json_data['id_order']
            Jenis_barang = json_data['jenis_barang']
            Dt_scrap = json_data['dt_scrap']
            Nama_barang = json_data['nama_barang']
            From_ = json_data['from_']
            To_ = json_data['to_']
            Pair_ = json_data['pair_']
            Diameter_ = json_data['diameter']
            Potongan_4m = json_data['potongan_4m']
            Potongan_sisa = json_data['potongan_sisa']
            Satuan = json_data['satuan']
            Jumlah = json_data['jumlah']
            Note = json_data['note']
            token = json_data['token']
            cek = verified_token(token)
            if cek == False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                input_hasil_scrap(Id_order,Jenis_barang,Dt_scrap,Nama_barang,From_,To_,Pair_,Diameter_,Potongan_4m,Potongan_sisa,
                                  Satuan,Jumlah,Note)
                result = {"message":"Input Success"}
                resp = jsonify(result)
                return resp,200
            
@app.route('/scrap/get/hasil+scrap',methods=['POST'])
def show_hasil_scrap():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'id_order' not in json_data or 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            Id_order = json_data['id_order']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_all_hasil_scrap(Id_order)
                return resp,200
            
            
@app.route('/scrap/get/hasil+scrap+detail',methods=['POST'])
def show_hasil_scrap_detail():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'id_scrap' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            Id_scrap = json_data['id_scrap']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_detail_hasil_scrap(Id_scrap)
                return resp,200
            
@app.route('/scrap/update/hasil+scrap',methods=['POST'])
def hasil_scrap_update():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'jenis_barang' not in json_data or 'dt_scrap' not in json_data or 'nama_barang' not in json_data or 'from_' not in json_data or 'to_' not in json_data or 'pair_' not in json_data or 'diameter' not in json_data or 'potongan_4m' not in json_data or 'potongan_sisa' not in json_data or 'satuan' not in json_data or 'jumlah' not in json_data or 'note' not in json_data or 'id_scrap' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Jenis_barang = json_data['jenis_barang']
            Dt_scrap = json_data['dt_scrap']
            Nama_barang = json_data['nama_barang']
            From_ = json_data['from_']
            To_ = json_data['to_']
            Pair_ = json_data['pair_']
            Diameter_ = json_data['diameter']
            Potongan_4M = json_data['potongan_4m']
            Potongan_sisa = json_data['potongan_sisa']
            Satuan = json_data['satuan']
            Jumlah = json_data['jumlah']
            Note = json_data['note']
            Id_scrap = json_data['id_scrap']
            token = json_data['token']
            cek = verified_token(token)
            if cek ==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                cek_id = cek_id_scrap(Id_scrap)
                if cek_id==False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    Update_by = get_actor_id_base_token(token)
                    update_hasil_scrap(Jenis_barang,Dt_scrap,Nama_barang,From_,To_,Pair_,
                                       Diameter_,Potongan_4M,Potongan_sisa,Satuan,Jumlah,
                                       Note,Update_by,Id_scrap)
                    result = {"message":"Update Success"}
                    resp = jsonify(result)
                    return resp,200

@app.route('/scrap/get/potensi',methods=['POST'])
def show_potensi():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek == False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                otoritas = int(get_otoritas_id_base_token(token))
                if otoritas<3:
                    regional = get_regional_user_base_on_token(token)
                    if regional==0:
                        resp = get_potensi_all()
                    else:
                        resp = get_potensi_base_on_regional(regional)
                    return resp,200
                else:
                    witel = get_witel_user_base_on_token(token)
                    regional = get_regional_user_base_on_token(token)
                    if regional == 0:
                        resp = get_potensi_all()
                        return resp,200
                    else:
                        if witel=="ALL":
                            resp = get_potensi_base_on_regional(regional)
                        else:
                            resp = get_potensi_base_on_witel(witel)
                        return resp,200
                        
@app.route('/scrap/get/potensi+detail',methods=['POST'])
def show_potensi_detail():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'Id_potensi' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            Id_potensi = json_data['Id_potensi']
            cek = verified_token(token)
            if cek == False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_potensi_detail(Id_potensi)
                return resp,200

@app.route('/scrap/input/potensi',methods=['POST'])
def potensi_input():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Sto' not in json_data or 'Jenis_barang' not in json_data or 'Lokasi' not in json_data or 'Nama_barang' not in json_data or 'Vendor' not in json_data or 'Satuan' not in json_data or 'Jumlah' not in json_data or 'Note' not in json_data or 'Lat' not in json_data or 'Lng' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            if 'Foto' not in json_data:
                Foto = None
            else:
                Foto = json_data['Foto']
            Sto = json_data['Sto']
            Jenis_barang = json_data['Jenis_barang']
            Lokasi = json_data['Lokasi']
            Nama_barang = json_data['Nama_barang']
            Vendor = json_data['Vendor']
            Satuan = json_data['Satuan']
            Jumlah = json_data['Jumlah']
            Note = json_data['Note']
            Lat = json_data['Lat']
            Lng = json_data['Lng']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                Add_by = get_otoritas_base_token(token)
                Update_by = Add_by
                input_potensi(Sto,Jenis_barang,Lokasi,Nama_barang,Vendor,Satuan,Jumlah,Note,Foto,Lat,Lng,Add_by,Update_by)
                result = {"message":"Input Success"}
                resp = jsonify(result)
                return resp,200

@app.route('/scrap/update/potensi',methods=['POST'])
def potensi_update():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Sto' not in json_data or 'Jenis_barang' not in json_data or 'Lokasi' not in json_data or 'Nama_barang' not in json_data or 'Vendor' not in json_data or 'Satuan' not in json_data or 'Jumlah' not in json_data or 'Note' not in json_data or 'Lat' not in json_data or 'Lng' not in json_data or 'Status' not in json_data or'token' not in json_data or 'Id_potensi' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            if 'Foto' not in json_data:
                Foto = None
            else:
                Foto = json_data['Foto']
            Id_potensi = json_data['Id_potensi']
            Sto = json_data['Sto']
            Jenis_barang = json_data['Jenis_barang']
            Lokasi = json_data['Lokasi']
            Nama_barang = json_data['Nama_barang']
            Vendor = json_data['Vendor']
            Satuan = json_data['Satuan']
            Jumlah = json_data['Jumlah']
            Note = json_data['Note']
            Lat = json_data['Lat']
            Lng = json_data['Lng']
            Status = json_data['Status']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                cek_id = cek_id_potensi(Id_potensi)
                if cek_id==False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    Update_by = get_actor_id_base_token(token)
                    update_potensi(Sto,Jenis_barang,Lokasi,Nama_barang,Vendor,Satuan,Jumlah,Note,Foto,Lat,Lng,Status,Update_by,Id_potensi)
                    cek_order = cek_existed_id_potensi_in_order_for_update_potensi(Id_potensi)
                    if cek_order==True:
                        update_status_order_in_update_potensi(Status,Update_by,Id_potensi)
                    result = {"message":"Update Success"}
                    resp = jsonify(result)
                    return resp,200

@app.route('/scrap/get/witel',methods=['POST'])
def show_witel():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'Regional' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            Regional = json_data['Regional']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_witel_base_regional(Regional)
                return resp,200

@app.route('/scrap/get/Lat&Lng',methods=['POST'])
def show_lat_lng():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed",
                  "Lat":"-",
                  "Lng":"-"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Sto' not in json_data or 'token' not in json_data:
            result = {"message":"error request",
                      "Lat":"-",
                      "Lng":"-"}
            resp = jsonify(result)
            return resp,401
        else:
            Sto = json_data['Sto']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden",
                  "Lat":"-",
                  "Lng":"-"}
                resp = jsonify(result)
                return resp,403
            else:
                data = get_lat_lng_base_sto(Sto)
                if data==None:
                    result = {"message":"No-Content",
                              "Lat":"-",
                              "Lng":"-"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    result = {"message":"Success",
                          "Lat":str(data[0]),
                          "Lng":str(data[1])}
                    resp = jsonify(result)
                    return resp,200
    
@app.route('/scrap/get/sto',methods=['POST'])
def show_sto():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'Witel' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            Witel = json_data['Witel']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_sto_base_witel(Witel)
                return resp,200

@app.route('/scrap/input/order',methods=['POST'])
def order_input():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Id_potensi' not in json_data or 'Mitra' not in json_data or 'Dt_order' not in json_data or 'Dt_target' not in json_data or 'Panjang' not in json_data or 'Jumlah' not in json_data or 'Satuan2' not in json_data or 'Jumlah2' not in json_data or 'Waspang_telkom' not in json_data or 'Waspang_mitra' not in json_data or 'Mitra_pelaksana' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            if 'Note' not in json_data:
                Note = "-"
            else:
                Note = json_data['Note']
            Id_potensi = json_data['Id_potensi']
            Mitra = json_data['Mitra']
            Dt_order = json_data['Dt_order']
            Dt_target = json_data['Dt_target']
            Panjang = json_data['Panjang']
            Jumlah = json_data['Jumlah']
            Satuan2 = json_data['Satuan2']
            Jumlah2 = json_data['Jumlah2']
            Waspang_telkom = json_data['Waspang_telkom']
            Waspang_mitra = json_data['Waspang_mitra']
            Mitra_pelaksana = json_data['Mitra_pelaksana']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                cek_potensi = cek_id_potensi(Id_potensi)
                if cek_potensi == False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else: 
                    Update_by = get_otoritas_base_token(token)
                    input_order(Id_potensi,Mitra,Dt_order,Dt_target,Panjang,Jumlah,Satuan2,
                                Jumlah2,Note,Waspang_telkom,Waspang_mitra,Mitra_pelaksana,Update_by)
                    result = {"message":"Input Success"}
                    resp = jsonify(result)
                    return resp,200
        
@app.route('/scrap/get/dashboard/teknik',methods=['POST'])
def show_dashboard():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            cek=verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                otoritas = get_otoritas_id_base_token(token)
                regional = get_regional_user_base_on_token(token)
                if regional==0:
                    resp = get_dashboard_teknik()
                    return resp,200
                else: 
                    if otoritas<3:
                        resp = get_dashboard_teknik_base_on_regional(regional)
                        return resp,200
                    else:
                        witel = get_witel_user_base_on_token(token)
                        if witel=="ALL":
                            resp = get_dashboard_teknik_base_on_regional(regional)
                        else:
                            resp = get_dashboard_teknik_for_waspang(witel)
                        return resp,200

@app.route('/scrap/search/dashboard/teknik',methods=['POST'])
def search_dashboard():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'value' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            value = json_data['value']
            cek=verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                otoritas = get_otoritas_id_base_token(token)
                regional = get_regional_user_base_on_token(token)
                if regional == 0:
                    resp = search_data_from_dashboard_teknik(value)
                    return resp,200
                else:
                    if otoritas<3:
                        resp = search_data_from_dashboard_teknik_base_on_regional(regional,value)
                        return resp,200
                    else:
                        witel = get_witel_user_base_on_token(token)
                        if witel=="ALL":
                            resp = search_data_from_dashboard_teknik_base_on_regional(regional,value)
                        else:
                            resp = search_data_from_dashboard_teknik_for_waspang(witel,value)
                        return resp,200
                
@app.route('/scrap/get/sto/all',methods=['POST'])
def all_sto():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_list_all_sto()
                return resp,200

@app.route('/scrap/input/sto',methods=['POST'])
def sto_input():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Sto' not in json_data or 'Nama_sto' not in json_data or 'Regional' not in json_data or 'Witel' not in json_data or 'Datel' not in json_data or 'Ubis' not in json_data or 'Lat' not in json_data or 'Lng' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Sto = json_data['Sto']
            Nama_sto = json_data['Nama_sto']
            Regional = json_data['Regional']
            Witel = json_data['Witel']
            Datel = json_data['Datel']
            Ubis = json_data['Ubis']
            Lat = json_data['Lat']
            Lng = json_data['Lng']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                input_sto(Sto,Nama_sto,Regional,Witel,Datel,Ubis,Lat,Lng)
                result = {"message":"Input Success"}
                resp = jsonify(result)
                return resp,200

@app.route('/scrap/update/sto',methods=['POST'])
def sto_update():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Sto' not in json_data or 'Nama_sto' not in json_data or 'Regional' not in json_data or 'Witel' not in json_data or 'Datel' not in json_data or 'Ubis' not in json_data or 'Lat' not in json_data or 'Lng' not in json_data or 'token' not in json_data or 'Sto_before' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Sto = json_data['Sto']
            Nama_sto = json_data['Nama_sto']
            Regional = json_data['Regional']
            Witel = json_data['Witel']
            Datel = json_data['Datel']
            Ubis = json_data['Ubis']
            Lat = json_data['Lat']
            Lng = json_data['Lng']
            Sto_before = json_data['Sto_before']
            token = json_data['token']
            cek = verified_token(token)
            if cek == False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                sto_cek = cek_existed_sto(Sto_before)
                if sto_cek==False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    update_sto(Sto,Nama_sto,Regional,Witel,Datel,Ubis,Lat,Lng,Sto_before)
                    result = {"message":"Update Success"}
                    resp = jsonify(result)
                    return resp,200

@app.route('/scrap/delete/sto',methods=['DELETE'])
def sto_delete():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Sto' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Sto = json_data['Sto']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                sto_cek = cek_existed_sto(Sto)
                if sto_cek==False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    delete_sto(Sto)
                    result = {"message":"Data deleted"}
                    resp = jsonify(result)
                    return resp,200

@app.route('/scrap/search/sto',methods=['POST'])
def sto_search():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'value' not in json_data or 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            value = json_data['value']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = search_sto(value)
                return resp,200

@app.route('/scrap/get/jenispotensi/all',methods=['POST'])
def all_jenispotensi():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_list_all_jenis_potensi()
                return resp,200

@app.route('/scrap/input/jenispotensi',methods=['POST'])
def jenispotensi_input():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Id_jenispotensi' not in json_data or 'Nama_jenispotensi' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Id_jenispotensi = json_data['Id_jenispotensi']
            Nama_jenispotensi = json_data['Nama_jenispotensi']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                input_jenis_potensi(Id_jenispotensi,Nama_jenispotensi)
                result = {"message":"Input success"}
                resp = jsonify(result)
                return resp,200
            
@app.route('/scrap/update/jenispotensi',methods=['POST'])
def jenispotensi_update():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Id_jenispotensi' not in json_data or 'Nama_jenispotensi' not in json_data or 'sortid' not in json_data or 'Id_jenispotensi_before' not in json_data or 'token'not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Id_jensipotensi = json_data['Id_jenispotensi']
            Nama_jenispotensi = json_data['Nama_jenispotensi']
            sortid = json_data['sortid']
            Id_jenispotensi_before = json_data['Id_jenispotensi_before']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                jenispotensi_cek = cek_existed_jenis_potensi(Id_jenispotensi_before)
                if jenispotensi_cek==False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    update_jenis_potensi(Id_jensipotensi,Nama_jenispotensi,sortid,Id_jenispotensi_before)
                    result = {"message":"Update Success"}
                    resp = jsonify(result)
                    return resp,200

@app.route('/scrap/delete/jenispotensi',methods=['DELETE'])
def jenispotensi_delete():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'Id_jenispotensi' not in json_data or 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            Id_jenispotensi = json_data['Id_jenispotensi']
            token = json_data['token']
            cek=verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                jenispotensi_cek = cek_existed_jenis_potensi(Id_jenispotensi)
                if jenispotensi_cek==False:
                    result = {"message":"No-content"}
                    resp = jsonify(result)
                    return resp,204
                else:
                    delete_jenis_potensi(Id_jenispotensi)
                    result = {"message":"Data deleted"}
                    resp = jsonify(result)
                    return resp,200

@app.route('/scrap/search/jenispotensi',methods=['POST'])
def jenispotensi_search():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'value' not in json_data or 'token' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            value = json_data['value']
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = search_jenis_potensi(value)
                return resp,200
                
@app.route('/scrap/user/changepassword',methods=['POST'])
def user_pass_change():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'User_id' not in json_data or 'Nama' not in json_data or 'Sandi' not in json_data or 'token' not in json_data or 'Sandi_old' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            User_id = json_data['User_id']
            Nama = json_data['Nama']
            Sandi_old = json_data['Sandi_old']
            Sandi = json_data['Sandi']
            Sandi = hashlib.sha256(str(Sandi).encode()).hexdigest()
            Sandi_old = hashlib.sha256(str(Sandi_old).encode()).hexdigest()
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                cek_pass_old = cek_user_pass_old_base_on_token(Sandi_old,User_id)
                if cek_pass_old==False:
                    result = {"message":"Wrong password old"}
                    resp = jsonify(result)
                    return resp,203
                else:
                    change_username_and_pass(Nama,Sandi,User_id)
                    result = {"message":"Password changed"}
                    resp = jsonify(result)
                    return resp,200
                    
@app.route('/scrap/user/get/data',methods=['POST'])
def data_user():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                result = get_data_user_name_id_base_on_token(token)
                resp = jsonify(result)
                return resp,200

@app.route('/scrap/user/get/regional',methods=['GET'])
def regional_user_show():
    data = get_regional_for_regist()
    return data,200

@app.route('/scrap/user/get/witel',methods=['POST'])
def witel_user_show():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'regional' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            regional = json_data['regional']
            resp = get_witel_base_regional(regional)
            return resp,200

@app.route('/scrap/get/general1/dashboard',methods=['POST'])
def general1_dashboard_show():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                potensi = general_dashboard_data_potensi()
                survey = general_dashboard_data_survey()
                scrap = general_dashboard_data_scrap()
                result = {"message":"Success",
                          "potensi":potensi,
                          "survey":survey,
                          "scrap":scrap}
                resp = jsonify(result)
                return resp,200
@app.route('/scrap/get/general2/dashboard',methods=['POST'])
def general2_dashboard_show():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                primer = general_dashboard_data_scrap_kt_primer()
                sekunder = general_dashboard_data_scrap_sekunder()
                kttl = general_dashboard_data_scrap_kttl()
                other = general_dashboard_data_scrap_other()
                result = {"message":"Success",
                          "primer":primer,
                          "sekunder":sekunder,
                          "kttl":kttl,
                          "other":other}
                resp = jsonify(result)
                return resp,200

@app.route('/scrap/get/general3/dashboard',methods=['POST'])
def general3_dashboard_show():
    json_data = request.json
    if json_data==None:
        result = {"message":"process failed"}
        resp = jsonify(result)
        return resp,400
    else:
        if 'token' not in json_data:
            result = {"message":"error request"}
            resp = jsonify(result)
            return resp,401
        else:
            token = json_data['token']
            cek = verified_token(token)
            if cek==False:
                result = {"message":"Forbidden"}
                resp = jsonify(result)
                return resp,403
            else:
                primer = general_dashboard_data_survey_kt_primer()
                sekunder = general_dashboard_data_survey_sekunder()
                kttl = general_dashboard_data_survey_kttl()
                other = general_dashboard_data_survey_other()
                result = {"message":"Success",
                          "primer":primer,
                          "sekunder":sekunder,
                          "kttl":kttl,
                          "other":other}
                resp = jsonify(result)
                return resp,200

@app.route('/scrap/get/rekap/potensi',methods=['POST'])
def rekap_potensi_get():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'tahun' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            tahun = json_data['tahun']
            cek = verified_token(token)
            if cek == False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_potensi_all_base_on_year(tahun)
                if 'regional' in json_data:
                    regional = json_data['regional']
                    resp = get_potensi_all_base_on_year_regional(tahun,regional)
                    if 'witel' in json_data:
                        witel = json_data['witel']
                        resp = get_potensi_all_base_on_year_regional_witel(tahun,regional,witel)
                return resp,200
            
@app.route('/scrap/get/rekap/scrap',methods=['POST'])
def rekap_scrap_get():
    json_data = request.json
    if json_data==None:
        result = []
        resp = json.dumps(result)
        return resp,400
    else:
        if 'token' not in json_data or 'date_start' not in json_data or 'date_end' not in json_data:
            result = []
            resp = json.dumps(result)
            return resp,401
        else:
            token = json_data['token']
            date_start = json_data['date_start']
            date_end = json_data['date_end']
            cek = verified_token(token)
            if cek==False:
                result = []
                resp = json.dumps(result)
                return resp,403
            else:
                resp = get_rekap_scrap_base_date_range(date_start,date_end)
                if 'regional' in json_data:
                    regional = json_data['regional']
                    resp = get_rekap_scrap_regional_and_date_range(date_start,date_end,regional)
                    if 'witel' in json_data:
                        witel = json_data['witel']
                        resp = get_rekap_scrap_regional_witel_and_date_range(date_start,date_end,regional,witel)                        
                return resp,200
                        
@app.route('/scrap/get/year/potensi',methods=['GET'])
def year_get():
    data = get_year_that_existed_in_potensi()
    return data,200

@app.route('/scrap/get/perusahaan',methods=['GET'])
def show_perusahaan():
    data = get_perusahaan()
    return data,200

@app.route('/scrap/welcome',methods=['GET'])
def welcome():
    result = {"message":"Welcome"}
    return result,200

if __name__ == '__main__':
    # serve(app, host="0.0.0.0", port=9001)
    app.run(port=9001, debug=True)
    # print(get_order_scrap())