import mysql.connector
import json
import random
import string
import hashlib
import datetime
from datetime import datetime
import secrets
from uuid import uuid4

from mysql.connector import cursor


def sql_connection():
    sql = mysql.connector.connect(host="localhost",
                                  user="root",
                                  password="",
                                  database="db_scrap")
    return sql

def input_user(user_id,nama,perusahaan,regional,witel,photo,sandi):
    db = sql_connection()
    cursor = db.cursor()
    otoritas = 4
    status_user = "UNCHECK"
    try:
        try:
            cursor.execute("insert into tb_user (User_id, Nama, Perusahaan, Regional, Witel, Otoritas, Photo, Sandi, Status_user,Dt_add, Dt_update) values (%s, %s, %s, %s, %s, %s, %s, %s, %s ,CURDATE(), null)",
                           (user_id, nama, perusahaan, regional,witel,otoritas,photo,sandi,status_user))
            db.commit()
        except(mysql.connector.Error,mysql.connector.Warning) as e :
            print(e) 
    finally:
        db.commit()
            
def cek_login_user(user_id,sandi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `User_id` FROM `tb_user` WHERE `User_id`=%s AND `Sandi`=%s",(user_id,sandi))
        
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        return False
    c = cursor.fetchone()
    if c == None:
        return False
    else:
        return True

def cek_user_exist(user_id):
    db = sql_connection()
    cursor = db.cursor()
    cursor.execute("SELECT `User_id` FROM `tb_user` WHERE `User_id`=%s",(user_id,))
    c = cursor.fetchone()
    if c == None:
        return True
    else:
        return False

def token_generator():
    return secrets.token_hex()

def update_token_base_otp(otp):
    db = sql_connection()
    cursor = db.cursor()
    token = token_generator()
    try:
        cursor.execute("UPDATE `tb_user` SET `token`=%s WHERE `Otp`=%s",(token,otp))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        token = None
    return token

def update_token_base_id(user_id,sandi):
    db = sql_connection()
    cursor = db.cursor()
    token = token_generator()
    try:
        cursor.execute("UPDATE `tb_user` SET `token`=%s ,`Dt_lastlogin`=CURDATE() , `Dt_expired`=ADDDATE(CURDATE(),INTERVAL 1 DAY) WHERE `User_id`=%s AND `Sandi`= %s",(token,user_id,sandi))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        token = None
    return token

def cek_status_user_base_id_sandi(userd_id):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Status_user` FROM `tb_user` WHERE `User_id`=%s",(userd_id,))
        c = cursor.fetchone()[0]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = "UNCHECK"
    if c==None or c == "UNCHECK":
        return False
    else:
        return True
    
def verified_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT  `token` FROM `tb_user` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return False
    else:
        return True

def generate_otp():
    generate_pass = ''.join([random.choice( string.ascii_uppercase +
                                            string.digits) 
                                            for n in range(4)])
    return generate_pass

def update_otp(user_id,sandi):
    db = sql_connection()
    cursor = db.cursor()
    otp = generate_otp()
    try:
        cursor.execute("UPDATE `tb_user` SET `Otp`=%s WHERE `User_id`=%s and `Sandi`=%s",(otp,user_id,sandi))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
    return otp

def update_dt_last_login(nama,sandi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_user` SET `Dt_lastlogin`=CURDATE() WHERE `Nama`= %s AND `Sandi`= %s",(nama,sandi))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def cek_otp_user(otp):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `User_id` FROM `tb_user` WHERE `Otp`=%s",(otp,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c == None:
        return False
    else:
        return True

def get_perusahaan():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_perusahaan`, `Nama_perusahaan` FROM `tb_perusahaan`")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    dataJson = json.dumps(datas)
    return dataJson
    

def get_date_last_login(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Dt_lastlogin` FROM `tb_user` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c == None:
        return None
    else:
        return c[0]
        
def get_date_expired(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Dt_expired` FROM `tb_user` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    
def compare_date(token):
    date_last_login = get_date_last_login(token)
    date_expired = get_date_expired(token)
    if date_last_login>=date_expired:
        return False
    else:
        return True
    
def update_last_login_base_on_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_user` SET `Dt_lastlogin`=CURDATE() WHERE `token`=%s",(token,))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)


def get_order_scrap():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Sto,tb_sto.Witel,tb_sto.Regional,tb_order_scrap.Dt_update,tb_potensi.Lokasi,tb_order_scrap.Mitra,tb_order_scrap.Panjang,tb_order_scrap.Id_order,tb_order_scrap.Id_potensi FROM `tb_order_scrap` INNER JOIN `tb_potensi` ON tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_update'] = str(datas[x]['Dt_update'])
    dataJson = json.dumps(datas)
    return dataJson

def get_order_scrap_base_on_regional(Regional):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Sto,tb_sto.Witel,tb_sto.Regional,tb_order_scrap.Dt_update,tb_potensi.Lokasi,tb_order_scrap.Mitra,tb_order_scrap.Panjang,tb_order_scrap.Id_order,tb_order_scrap.Id_potensi FROM `tb_order_scrap` INNER JOIN `tb_potensi` ON tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Regional =%s",(Regional,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_update'] = str(datas[x]['Dt_update'])
    dataJson = json.dumps(datas)
    return dataJson



def get_order_scrap_base_on_witel(witel):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Sto,tb_sto.Witel,tb_sto.Regional,tb_order_scrap.Dt_update,tb_potensi.Lokasi,tb_order_scrap.Mitra,tb_order_scrap.Panjang,tb_order_scrap.Id_order,tb_order_scrap.Id_potensi FROM `tb_order_scrap` INNER JOIN `tb_potensi` ON tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Witel=%s",(witel,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_update'] = str(datas[x]['Dt_update'])
    dataJson = json.dumps(datas)
    return dataJson

def cek_order_scrap(Id_order,Id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_order`,`Id_potensi` FROM `tb_order_scrap` WHERE `Id_order`=%s AND `Id_potensi`=%s",(Id_order,Id_potensi))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c == None:
        return False
    else:
        return True

def get_order_scrap_detail(Id_order,Id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_order`,`Id_potensi`,`Mitra`,`Dt_order`,`Dt_target`,`Panjang`,`Jumlah`,`Satuan2`,`Jumlah2`,`Note`,`Waspang_telkom`,`Waspang_mitra`,`Mitra_pelaksana`,`Status_order` FROM `tb_order_scrap` WHERE `Id_order`= %s AND `Id_potensi`= %s",(Id_order,Id_potensi))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    hasil = datas[0]
    hasil['Dt_order'] = str(hasil['Dt_order'])
    hasil['Dt_target'] = str(hasil['Dt_target'])
    datas[0] = hasil
    dataJson = json.dumps(datas)
    return dataJson


def update_order_scrap(mitra,panjang,jumlah,satuan2,jumlah2,note,waspang_telkom,waspang_mitra,mitra_pelaksana,status_order,update_by,id_order,id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_order_scrap` SET `Mitra`=%s,`Panjang`=%s,`Jumlah`=%s,`Satuan2`=%s,`Jumlah2`=%s,`Note`=%s,`Waspang_telkom`=%s,`Waspang_mitra`=%s,`Mitra_pelaksana`=%s,`Status_order`=%s,`Update_by`=%s,`Dt_update`= now() WHERE `Id_order`=%s AND `Id_potensi`=%s",(mitra,panjang,jumlah,satuan2,jumlah2,note,waspang_telkom,waspang_mitra,mitra_pelaksana,status_order,update_by,id_order,id_potensi))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
    
def get_otoritas_base_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `tb_otoritas`.`Oto_name` FROM `tb_user` INNER JOIN `tb_otoritas` ON `tb_user`.`Otoritas`=`tb_otoritas`.`Oto_id` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c == None:
        return None
    else:
        return c[0]
    

def gen_id_scrap():
    now = datetime.now()
    tahun = now.strftime("%y")
    bulan = now.strftime("%m")
    data = str(uuid4().hex)[:12]
    result = "S-"+tahun+bulan+data
    return result

def gen_id_potensi():
    data = str(uuid4().hex)[:11]
    result = "W-60"+data
    return result
        
def input_hasil_scrap(Id_order,Jenis_barang,Dt_scrap,Nama_barang,From_,To_,Pair_,Diameter_,Potongan_4M,Potongan_sisa,Satuan,Jumlah,Note):
    db = sql_connection()
    cursor = db.cursor()
    Id_scrap = gen_id_scrap()
    try:
        cursor.execute("INSERT INTO `tb_hasil_scrap`(`Id_scrap`, `Id_order`, `Jenis_barang`, `Dt_scrap`, `Nama_barang`, `From_`, `To_`, `Pair_`, `Diameter_`, `Potongan_4M`, `Potongan_sisa`, `Satuan`, `Jumlah`, `Note`, `Dt_add`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,now())",(Id_scrap,Id_order,Jenis_barang,Dt_scrap,Nama_barang,From_,To_,Pair_,Diameter_,Potongan_4M,Potongan_sisa,Satuan,Jumlah,Note))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)  


def update_hasil_scrap(Jenis_barang,Dt_scrap,Nama_barang,From_,To_,Pair_,Diameter_,Potongan_4M,Potongan_sisa,Satuan,Jumlah,Note,Update_by,Id_scrap):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_hasil_scrap` SET `Jenis_barang`=%s,`Dt_scrap`=%s,`Nama_barang`=%s,`From_`=%s,`To_`=%s,`Pair_`=%s,`Diameter_`=%s,`Potongan_4M`=%s,`Potongan_sisa`=%s,`Satuan`=%s,`Jumlah`=%s,`Note`=%s,`Update_by`=%s,`Dt_update`=now() WHERE `Id_scrap`=%s",(Jenis_barang,Dt_scrap,Nama_barang,From_,To_,Pair_,Diameter_,Potongan_4M,Potongan_sisa,Satuan,Jumlah,Note,Update_by,Id_scrap))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
    
        

def get_all_hasil_scrap(Id_order):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_scrap`, `Id_order`, `Jenis_barang`, `Dt_scrap`, `Nama_barang`, `Satuan`, `Jumlah`, `Sts_check` FROM `tb_hasil_scrap` WHERE `Id_order`=%s",(Id_order,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_scrap'] = str(datas[x]['Dt_scrap'])
        datas[x]['Jumlah'] = str(datas[x]['Jumlah'])
    dataJson = json.dumps(datas)
    return dataJson
    

def get_detail_hasil_scrap(Id_scrap):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_scrap`, `Id_order`, `Jenis_barang`, `Dt_scrap`, `Nama_barang`, `From_`, `To_`, `Pair_`, `Diameter_`, `Potongan_4M`, `Potongan_sisa`, `Satuan`, `Jumlah`, `Note` FROM `tb_hasil_scrap` WHERE `Id_scrap`=%s",(Id_scrap,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_scrap'] = str(datas[x]['Dt_scrap'])
        datas[x]['Jumlah'] = str(datas[x]['Jumlah'])
    dataJson = json.dumps(datas)
    return dataJson    
    

def cek_id_scrap(Id_scrap):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_scrap` FROM `tb_hasil_scrap` WHERE `Id_scrap`=%s",(Id_scrap,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return False
    else:
        return True

def get_potensi_all():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_potensi`, `Sto`, `Jenis_barang`, `Lokasi`, `Nama_barang`,`Satuan`, `Jumlah`, `Lat`, `Lng`,`Status` FROM `tb_potensi`")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson

def get_potensi_base_on_regional(Regional):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Id_potensi,tb_potensi.Sto,tb_potensi.Jenis_barang,tb_potensi.Lokasi,tb_potensi.Nama_barang,tb_potensi.Satuan,tb_potensi.Jumlah,tb_potensi.Lat,tb_potensi.Lng,tb_potensi.Status FROM `tb_potensi` INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Regional = %s",(Regional,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson

def get_potensi_base_on_witel(Witel):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Id_potensi,tb_potensi.Sto,tb_potensi.Jenis_barang,tb_potensi.Lokasi,tb_potensi.Nama_barang,tb_potensi.Satuan,tb_potensi.Jumlah,tb_potensi.Lat,tb_potensi.Lng,tb_potensi.Status FROM `tb_potensi` INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Witel = %s",(Witel,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson

# def cek_witel_user_base_token(token):
    
    
def get_potensi_detail(Id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_potensi`, `Sto`, `Jenis_barang`, `Lokasi`, `Nama_barang`, `Vendor`,`Satuan`, `Jumlah`, `Note`,`Lat`, `Lng`, tb_status_order.Sts_order FROM `tb_potensi` INNER JOIN tb_status_order ON tb_potensi.Status = tb_status_order.Id_status WHERE `Id_potensi`=%s",(Id_potensi,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
        datas[x]['Witel']=get_witel_regional_base_sto(datas[x]['Sto'])[1]
        datas[x]['Regional']=get_witel_regional_base_sto(datas[x]['Sto'])[0]
    dataJson = json.dumps(datas)
    return dataJson

def get_witel_regional_base_sto(Sto):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Regional`, `Witel` FROM `tb_sto` WHERE `Sto`=%s",(Sto,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c
    

def input_potensi(Sto,Jenis_barang,Lokasi,Nama_barang,Vendor,Satuan,Jumlah,Note,Foto,Lat,Lng,Add_by,Update_by):
    db = sql_connection()
    cursor = db.cursor()
    Id_potensi = gen_id_potensi()
    try:
        cursor.execute("INSERT INTO `tb_potensi`(`Id_potensi`, `Sto`, `Jenis_barang`, `Lokasi`, `Nama_barang`, `Vendor`, `Satuan`, `Jumlah`, `Note`, `Foto`, `Lat`, `Lng`, `Status`, `Dt_add`, `Add_by`, `Dt_update`, `Update_by`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,0,now(),%s,now(),%s)",(Id_potensi,Sto,Jenis_barang,Lokasi,Nama_barang,Vendor,Satuan,Jumlah,Note,Foto,Lat,Lng,Add_by,Update_by))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
    return Id_potensi

def update_potensi(Sto,Jenis_barang,Lokasi,Nama_barang,Vendor,Satuan,Jumlah,Note,Foto,Lat,Lng,Status,Update_by,Id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_potensi` SET `Sto`= %s,`Jenis_barang`= %s,`Lokasi`= %s,`Nama_barang`= %s,`Vendor`= %s,`Satuan`= %s,`Jumlah`= %s,`Note`= %s,`Foto`= %s,`Lat`= %s,`Lng`= %s,`Status`=%s,`Dt_update`= now(),`Update_by`= %s WHERE `Id_potensi`=%s",(Sto,Jenis_barang,Lokasi,Nama_barang,Vendor,Satuan,Jumlah,Note,Foto,Lat,Lng,Status,Update_by,Id_potensi))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        

def cek_id_potensi(Id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_potensi` FROM `tb_potensi` WHERE `Id_potensi`=%s",(Id_potensi,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c == None:
        return False
    else:
        return True
    

def get_witel_base_regional(Regional):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT DISTINCT `Witel` FROM `tb_sto` WHERE `Regional`=%s",(Regional,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    dataJson = json.dumps(datas)
    return dataJson

def get_sto_base_witel(Witel):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Sto`  FROM `tb_sto` WHERE `Witel`=%s",(Witel,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    dataJson = json.dumps(datas)
    return dataJson

def get_lat_lng_base_sto(Sto):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Lat`, `Lng` FROM `tb_sto` WHERE `Sto`=%s",(Sto,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e :
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c
    

def gen_id_order():
    data = str(uuid4().hex)[:10]
    result = "W-608"+data
    return result

def input_order(Id_potensi,Mitra,Dt_order,Dt_target,Panjang,Jumlah,Satuan2,Jumlah2,Note,Waspang_telkom,Waspang_mitra,Mitra_pelaksana,Update_by):
    db = sql_connection()
    cursor = db.cursor()
    Id_order = gen_id_order()
    try:
        cursor.execute("INSERT INTO `tb_order_scrap`(`Id_order`, `Id_potensi`, `Mitra`, `Dt_order`, `Dt_target`, `Panjang`, `Jumlah`, `Satuan2`, `Jumlah2`, `Note`, `Waspang_telkom`, `Waspang_mitra`, `Mitra_pelaksana`, `Status_order`, `Dt_add`, `Update_by`, `Dt_update`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,0,now(),%s,now())",
                       (Id_order,Id_potensi,Mitra,Dt_order,Dt_target,Panjang,Jumlah,Satuan2,Jumlah2,Note,Waspang_telkom,Waspang_mitra,Mitra_pelaksana,Update_by))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def get_dashboard_teknik():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_order_scrap.Id_order,tb_order_scrap.Status_order,tb_potensi.Sto,tb_potensi.Lokasi FROM `tb_potensi` INNER JOIN tb_order_scrap on tb_potensi.Id_potensi = tb_order_scrap.Id_potensi")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Status_order'] = round((datas[x]['Status_order']/6)*100)
        
    dataJson = json.dumps(datas)
    return dataJson

def get_dashboard_teknik_base_on_regional(Regional):
    db =  sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_order_scrap.Id_order,tb_order_scrap.Status_order,tb_potensi.Sto,tb_potensi.Lokasi FROM `tb_potensi` INNER JOIN tb_order_scrap on tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Regional=%s",(Regional,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Status_order'] = round((datas[x]['Status_order']/6)*100)
        
    dataJson = json.dumps(datas)
    return dataJson

def get_dashboard_teknik_for_waspang(witel):
    db =  sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_order_scrap.Id_order,tb_order_scrap.Status_order,tb_potensi.Sto,tb_potensi.Lokasi FROM `tb_potensi` INNER JOIN tb_order_scrap on tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Witel=%s",(witel,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Status_order'] = round((datas[x]['Status_order']/6)*100)
        
    dataJson = json.dumps(datas)
    return dataJson

def search_data_from_dashboard_teknik(value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_order_scrap.Id_order,tb_order_scrap.Status_order,tb_potensi.Sto,tb_potensi.Lokasi FROM `tb_potensi` INNER JOIN tb_order_scrap on tb_potensi.Id_potensi = tb_order_scrap.Id_potensi WHERE concat(tb_potensi.Sto,tb_potensi.Lokasi,tb_order_scrap.Id_order) LIKE %s",("%" + value + "%", ))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Status_order'] = round((datas[x]['Status_order']/6)*100)
        
    dataJson = json.dumps(datas)
    return dataJson
    
def search_data_from_dashboard_teknik_base_on_regional(Regional,value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_order_scrap.Id_order,tb_order_scrap.Status_order,tb_potensi.Sto,tb_potensi.Lokasi FROM `tb_potensi` INNER JOIN tb_order_scrap on tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Regional= %s and concat(tb_potensi.Sto,tb_potensi.Lokasi,tb_order_scrap.Id_order) LIKE  %s",(Regional,"%" + value + "%" ))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Status_order'] = round((datas[x]['Status_order']/6)*100)
        
    dataJson = json.dumps(datas)
    return dataJson

def search_data_from_dashboard_teknik_for_waspang(witel,value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_order_scrap.Id_order,tb_order_scrap.Status_order,tb_potensi.Sto,tb_potensi.Lokasi FROM `tb_potensi` INNER JOIN tb_order_scrap on tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE tb_sto.Witel= %s and concat(tb_potensi.Sto,tb_potensi.Lokasi,tb_order_scrap.Id_order) LIKE  %s",(witel,"%" + value + "%" ))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Status_order'] = round((datas[x]['Status_order']/6)*100)
        
    dataJson = json.dumps(datas)
    return dataJson


def get_list_all_sto():
    db = sql_connection()
    cursor =  db.cursor()
    try:
        cursor.execute("SELECT `Sto`, `Nama_sto`, `Regional`, `Witel`, `Datel`, `Ubis`, `Lat`, `Lng` FROM `tb_sto`")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
        
    dataJson = json.dumps(datas)
    return dataJson

def input_sto(Sto,Nama_sto,Regional,Witel,Datel,Ubis,Lat,Lng):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("INSERT INTO `tb_sto`(`Sto`, `Nama_sto`, `Regional`, `Witel`, `Datel`, `Ubis`, `Lat`, `Lng`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",(Sto,Nama_sto,Regional,Witel,Datel,Ubis,Lat,Lng))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        
def update_sto(Sto,Nama_sto,Regional,Witel,Datel,Ubis,Lat,Lng,Sto_before):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_sto` SET `Sto`=%s,`Nama_sto`=%s,`Regional`=%s,`Witel`=%s,`Datel`=%s,`Ubis`=%s,`Lat`=%s,`Lng`=%s WHERE `Sto`=%s",(Sto,Nama_sto,Regional,Witel,Datel,Ubis,Lat,Lng,Sto_before))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def cek_existed_sto(Sto):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Sto` FROM `tb_sto` WHERE `Sto`=%s",(Sto,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return False
    else:
        return True
    
def delete_sto(Sto):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("DELETE FROM `tb_sto` WHERE `Sto`=%s",(Sto,))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        
def search_sto(value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Sto`, `Nama_sto`, `Regional`, `Witel`, `Datel`, `Ubis`, `Lat`, `Lng` FROM `tb_sto` WHERE concat(Sto,Nama_sto,Regional,Witel,Datel,Ubis) LIKE %s",("%" + value + "%", ))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
        
    dataJson = json.dumps(datas)
    return dataJson

def get_list_all_jenis_potensi():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_jenispotensi`, `Nama_jenispotensi`, `Sortid` FROM `tb_jenis_potensi` ORDER BY Sortid ASC")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    
    dataJson = json.dumps(datas)
    return dataJson


def get_max_value_sortid_jensi_potensi():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT MAX(Sortid) FROM tb_jenis_potensi")
        c = cursor.fetchone()[0]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = 0
    return c

def input_jenis_potensi(Id_jenispotensi,Nama_jenispotensi):
    db = sql_connection()
    cursor = db.cursor()
    sortid = int(get_max_value_sortid_jensi_potensi())+1
    try:
        cursor.execute("INSERT INTO `tb_jenis_potensi`(`Id_jenispotensi`, `Nama_jenispotensi`, `Sortid`) VALUES (%s,%s,%s)",(Id_jenispotensi,Nama_jenispotensi,sortid))
        db.commit()
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)

def search_jenis_potensi(value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_jenispotensi`, `Nama_jenispotensi`, `Sortid` FROM `tb_jenis_potensi` WHERE concat(Id_jenispotensi,Nama_jenispotensi) LIKE %s",("%" + value + "%", ))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    
    dataJson = json.dumps(datas)
    return dataJson

def cek_existed_jenis_potensi(Id_jenispotensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_jenispotensi` FROM `tb_jenis_potensi` WHERE `Id_jenispotensi`=%s",(Id_jenispotensi,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return False
    else:
        return True

def update_jenis_potensi(Id_jenispotensi,Nama_jenispotensi,sortid,id_jenispotensi_before):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_jenis_potensi` SET `Id_jenispotensi`=%s,`Nama_jenispotensi`=%s,`Sortid`=%s WHERE `Id_jenispotensi`=%s",(Id_jenispotensi,Nama_jenispotensi,sortid,id_jenispotensi_before))
        db.commit()
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)


def delete_jenis_potensi(Id_jenispotensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("DELETE FROM `tb_jenis_potensi` WHERE `Id_jenispotensi`=%s",(Id_jenispotensi,))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)


def get_list_satuan_potensi():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Satuan`, `Sortid` FROM `tb_satuan_potensi` ORDER BY Sortid ASC")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    
    dataJson = json.dumps(datas)
    return dataJson

def get_max_sortid_satuan_potensi():
    db =sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT MAX(Sortid) FROM `tb_satuan_potensi`")
        c = cursor.fetchone()[0]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = 0
    return c

def input_satuan_potensi(Satuan):
    db = sql_connection()
    cursor = db.cursor()
    SortId = int(get_max_sortid_satuan_potensi())+1
    try:
        cursor.execute("INSERT INTO `tb_satuan_potensi`(`Satuan`, `Sortid`) VALUES (%s,%s)",(Satuan,SortId))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def update_satuan_potensi(Satuan,SortId,Satuan_before):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_satuan_potensi` SET `Satuan`=%s,`Sortid`=%s WHERE `Satuan`=%s",(Satuan,SortId,Satuan_before))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def delete_satuan_potensi(Satuan):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("DELETE FROM `tb_satuan_potensi` WHERE `Satuan`=%s",(Satuan,))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def get_list_all_perusahaan():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_perusahaan`, `Nama_perusahaan`, `Sortid` FROM `tb_perusahaan`")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    
    dataJson = json.dumps(datas)
    return dataJson

def get_max_sortid_perusahaan():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT  MAX(`Sortid`) FROM `tb_perusahaan`")
        c = cursor.execute()[0]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = 0
    return c

def input_perusahaan(Id_perusahaan,Nama_perusahaan):
    db = sql_connection()
    cursor = db.cursor()
    Sortid = int(get_max_sortid_perusahaan())+1
    try:
        cursor.execute("INSERT INTO `tb_perusahaan`(`Id_perusahaan`, `Nama_perusahaan`, `Sortid`) VALUES (%s,%s,%s)",(Id_perusahaan,Nama_perusahaan,Sortid))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def update_perusahaan(Id_perusahaan,Nama_perusahaan,Sortid,Id_perusahaan_before):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_perusahaan` SET `Id_perusahaan`=%s,`Nama_perusahaan`=%s,`Sortid`=%s WHERE `Id_perusahaan`=%s",(Id_perusahaan,Nama_perusahaan,Sortid,Id_perusahaan_before))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        
def delete_perusahaan(Id_perusahaan):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("DELETE FROM `tb_perusahaan` WHERE `Id_perusahaan`=%s",(Id_perusahaan,))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def change_username_and_pass(Nama,Sandi,User_id):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_user` SET `Nama`=%s,`Sandi`=%s , `Dt_chgpass`= now() ,`Dt_update`= now() WHERE `User_id`=%s",(Nama,Sandi,User_id))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        
def get_data_user_name_id_base_on_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `User_id`, `Nama` FROM `tb_user` WHERE `token`=%s",(token,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    
    dataJson = datas[0]
    return dataJson

def cek_user_pass_old_base_on_token(Sandi,User_id):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT  `User_id` FROM `tb_user` WHERE `User_id`= %s AND `Sandi`= %s",(User_id,Sandi))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return False
    else:
        return True
    
def get_regional_for_regist():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT DISTINCT `Regional` FROM `tb_sto`")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    dataJson = json.dumps(datas)
    return dataJson

def get_otoritas_id_base_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Otoritas` FROM `tb_user` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    

def get_witel_user_base_on_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Witel` FROM `tb_user` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)
        c = None
    if c == None:
        return None
    else:
        return c[0]
    
def get_actor_id_base_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `User_id` FROM `tb_user` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)
        c = None
    if c ==None:
        return None
    else:
        return c[0]
    
def update_status_potensi(id_potensi,Update_by,status):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_potensi` SET `Status`= %s,`Update_by`= %s , `Dt_update`= now() WHERE `Id_potensi`=%s",(status,Update_by,id_potensi))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def cek_existed_id_potensi_in_order_for_update_potensi(id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT  `Id_potensi` FROM `tb_order_scrap` WHERE `Id_potensi`=%s",(id_potensi,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return False
    else:
        return True

def update_status_order_in_update_potensi(Status_order,Update_by,id_potensi):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_order_scrap` SET `Status_order`= %s,`Update_by`=%s,`Dt_update`=now() WHERE `Id_potensi`=%s",(Status_order,Update_by,id_potensi))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        
def get_regional_user_base_on_token(token):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Regional` FROM `tb_user` WHERE `token`=%s",(token,))
        c = cursor.fetchone()
    except(mysql.connector.Warning,mysql.connector.Error) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    
def general_dashboard_data_potensi():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi`")
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
 
def general_dashboard_data_survey():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=3")
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]

def general_dashboard_data_scrap():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=5")
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    
def general_dashboard_data_survey_kt_primer():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KT_PRIMER"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=3 AND `Jenis_barang`=%s",(barang,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    

def general_dashboard_data_survey_kttl():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KTTL"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=3 AND `Jenis_barang`=%s",(barang,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]


def general_dashboard_data_survey_sekunder():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KT_SEKUNDER"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=3 AND `Jenis_barang`=%s",(barang,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]

def general_dashboard_data_survey_other():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KT_SEKUNDER"
    barang_1 = "KTTL"
    barang_2 = "KT_PRIMER"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=3 AND `Jenis_barang`!=%s AND `Jenis_barang`!=%s AND `Jenis_barang`!=%s ",(barang,barang_1,barang_2))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    

def general_dashboard_data_scrap_kt_primer():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KT_PRIMER"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=5 AND `Jenis_barang`=%s",(barang,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    

def general_dashboard_data_scrap_kttl():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KTTL"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=5 AND `Jenis_barang`=%s",(barang,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]


def general_dashboard_data_scrap_sekunder():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KT_SEKUNDER"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=3 AND `Jenis_barang`=%s",(barang,))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]

def general_dashboard_data_scrap_other():
    db = sql_connection()
    cursor = db.cursor()
    barang = "KT_SEKUNDER"
    barang_1 = "KTTL"
    barang_2 = "KT_PRIMER"
    try:
        cursor.execute("SELECT COUNT(Id_potensi) FROM `tb_potensi` WHERE `Status`=3 AND `Jenis_barang`!=%s AND `Jenis_barang`!=%s AND `Jenis_barang`!=%s ",(barang,barang_1,barang_2))
        c = cursor.fetchone()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        c = None
    if c==None:
        return None
    else:
        return c[0]
    
def get_potensi_all_base_on_year(year):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.`Id_potensi`, tb_potensi.`Sto`, tb_potensi.`Jenis_barang`, tb_potensi.`Lokasi`, tb_potensi.`Nama_barang`,tb_potensi.`Satuan`, tb_potensi.`Jumlah`, tb_potensi.`Lat`, tb_potensi.`Lng`,tb_status_order.Sts_order FROM `tb_potensi` JOIN tb_status_order ON tb_status_order.Id_status = tb_potensi.Status WHERE YEAR(Dt_add) = %s",(year,))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson


def get_potensi_all_base_on_year_regional(year,regional):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.`Id_potensi`, tb_potensi.`Sto`, tb_potensi.`Jenis_barang`, tb_potensi.`Lokasi`, tb_potensi.`Nama_barang`,tb_potensi.`Satuan`, tb_potensi.`Jumlah`, tb_potensi.`Lat`, tb_potensi.`Lng`,tb_status_order.Sts_order FROM `tb_potensi` JOIN tb_status_order ON tb_status_order.Id_status = tb_potensi.Status JOIN tb_sto ON tb_sto.Sto = tb_potensi.Sto WHERE YEAR(Dt_add) = %s AND tb_sto.Regional = %s",(year,regional))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson

def get_potensi_all_base_on_year_regional_witel(year,regional,witel):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.`Id_potensi`, tb_potensi.`Sto`, tb_potensi.`Jenis_barang`, tb_potensi.`Lokasi`, tb_potensi.`Nama_barang`,tb_potensi.`Satuan`, tb_potensi.`Jumlah`, tb_potensi.`Lat`, tb_potensi.`Lng`,tb_status_order.Sts_order FROM `tb_potensi` JOIN tb_status_order ON tb_status_order.Id_status = tb_potensi.Status JOIN tb_sto ON tb_sto.Sto = tb_potensi.Sto WHERE YEAR(Dt_add) = %s AND tb_sto.Regional = %s AND tb_sto.Witel = %s",(year,regional,witel))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson

def get_year_that_existed_in_potensi():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT DISTINCT YEAR(Dt_add) as Tahun FROM `tb_potensi`")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    dataJson = json.dumps(datas)
    return dataJson

def get_rekap_scrap_all():
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_sto.Sto,tb_hasil_scrap.Dt_scrap ,tb_potensi.Id_potensi,tb_order_scrap.Id_order,SUM(tb_hasil_scrap.Jumlah) as Jumlah , tb_hasil_scrap.Note FROM tb_hasil_scrap JOIN tb_order_scrap ON tb_order_scrap.Id_order = tb_hasil_scrap.Id_order JOIN tb_potensi ON tb_order_scrap.Id_potensi = tb_potensi.Id_potensi JOIN tb_sto ON tb_sto.Sto=tb_potensi.Sto GROUP BY tb_hasil_scrap.Dt_scrap , tb_order_scrap.Id_order,tb_potensi.Id_potensi")
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Dt_scrap'] = str(datas[x]['Dt_scrap'])
        datas[x]['Jumlah'] = str(datas[x]['Jumlah'])
    dataJson = json.dumps(datas)
    return dataJson

def get_rekap_scrap_base_date_range(dt_start,dt_end):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_sto.Sto,tb_hasil_scrap.Dt_scrap ,tb_potensi.Id_potensi,tb_order_scrap.Id_order,SUM(tb_hasil_scrap.Jumlah) as Jumlah , tb_hasil_scrap.Sts_check,tb_hasil_scrap.Note FROM tb_hasil_scrap JOIN tb_order_scrap ON tb_order_scrap.Id_order = tb_hasil_scrap.Id_order JOIN tb_potensi ON tb_order_scrap.Id_potensi = tb_potensi.Id_potensi JOIN tb_sto ON tb_sto.Sto=tb_potensi.Sto  WHERE tb_hasil_scrap.Dt_scrap BETWEEN %s AND %s GROUP BY tb_hasil_scrap.Dt_scrap , tb_order_scrap.Id_order,tb_potensi.Id_potensi",(dt_start,dt_end))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Dt_scrap'] = str(datas[x]['Dt_scrap'])
        datas[x]['Jumlah'] = str(datas[x]['Jumlah'])
    dataJson = json.dumps(datas)
    return dataJson
        
def get_rekap_scrap_regional_and_date_range(dt_start,dt_end,regional):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_sto.Sto,tb_hasil_scrap.Dt_scrap ,tb_potensi.Id_potensi,tb_order_scrap.Id_order,SUM(tb_hasil_scrap.Jumlah) as Jumlah ,tb_hasil_scrap.Sts_check ,tb_hasil_scrap.Note FROM tb_hasil_scrap JOIN tb_order_scrap ON tb_order_scrap.Id_order = tb_hasil_scrap.Id_order JOIN tb_potensi ON tb_order_scrap.Id_potensi = tb_potensi.Id_potensi JOIN tb_sto ON tb_sto.Sto=tb_potensi.Sto  WHERE tb_sto.Regional=%s AND (tb_hasil_scrap.Dt_scrap BETWEEN %s AND %s)  GROUP BY tb_hasil_scrap.Dt_scrap , tb_order_scrap.Id_order,tb_potensi.Id_potensi",(regional,dt_start,dt_end))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Dt_scrap'] = str(datas[x]['Dt_scrap'])
        datas[x]['Jumlah'] = str(datas[x]['Jumlah'])
    dataJson = json.dumps(datas)
    return dataJson

def get_rekap_scrap_regional_witel_and_date_range(dt_start,dt_end,regional,witel):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_sto.Sto,tb_hasil_scrap.Dt_scrap ,tb_potensi.Id_potensi,tb_order_scrap.Id_order,SUM(tb_hasil_scrap.Jumlah) as Jumlah ,tb_hasil_scrap.Sts_check ,tb_hasil_scrap.Note FROM tb_hasil_scrap JOIN tb_order_scrap ON tb_order_scrap.Id_order = tb_hasil_scrap.Id_order JOIN tb_potensi ON tb_order_scrap.Id_potensi = tb_potensi.Id_potensi JOIN tb_sto ON tb_sto.Sto=tb_potensi.Sto  WHERE tb_sto.Regional=%s AND (tb_hasil_scrap.Dt_scrap BETWEEN %s AND %s) AND tb_sto.Witel = %s GROUP BY tb_hasil_scrap.Dt_scrap , tb_order_scrap.Id_order,tb_potensi.Id_potensi",(regional,dt_start,dt_end,witel))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
    for x in range(0,len(datas)):
        datas[x]['Dt_scrap'] = str(datas[x]['Dt_scrap'])
        datas[x]['Jumlah'] = str(datas[x]['Jumlah'])
    dataJson = json.dumps(datas)
    return dataJson


def input_evidence(id_ev,nama_file):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("INSERT INTO `tb_evidence`(`id`, `nama_file`, `dt_upload`) VALUES (%s,%s,now())",(id_ev,nama_file))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)

def update_evidence(id_ev,nama_file):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE `tb_evidence` SET `nama_file`=%s,`dt_upload`=now() WHERE `id`= %s",(nama_file,id_ev))
        db.commit()
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)


def search_get_order_scrap(value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Sto,tb_sto.Witel,tb_sto.Regional,tb_order_scrap.Dt_update,tb_potensi.Lokasi,tb_order_scrap.Mitra,tb_order_scrap.Panjang,tb_order_scrap.Id_order,tb_order_scrap.Id_potensi FROM `tb_order_scrap` INNER JOIN `tb_potensi` ON tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE concat(tb_potensi.Sto,tb_sto.Witel,tb_potensi.Lokasi,tb_order_scrap.Id_order) LIKE %s",("%"+value+"%",))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_update'] = str(datas[x]['Dt_update'])
    dataJson = json.dumps(datas)
    return dataJson


def search_get_order_scrap_base_on_regional(regional,value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Sto,tb_sto.Witel,tb_sto.Regional,tb_order_scrap.Dt_update,tb_potensi.Lokasi,tb_order_scrap.Mitra,tb_order_scrap.Panjang,tb_order_scrap.Id_order,tb_order_scrap.Id_potensi FROM `tb_order_scrap` INNER JOIN `tb_potensi` ON tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE concat(tb_potensi.Sto,tb_sto.Witel,tb_potensi.Lokasi,tb_order_scrap.Id_order) LIKE %s AND tb_sto.Regional = %s",("%"+value+"%",regional))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_update'] = str(datas[x]['Dt_update'])
    dataJson = json.dumps(datas)
    return dataJson

def search_get_order_scrap_base_on_witel(witel,value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Sto,tb_sto.Witel,tb_sto.Regional,tb_order_scrap.Dt_update,tb_potensi.Lokasi,tb_order_scrap.Mitra,tb_order_scrap.Panjang,tb_order_scrap.Id_order,tb_order_scrap.Id_potensi FROM `tb_order_scrap` INNER JOIN `tb_potensi` ON tb_potensi.Id_potensi = tb_order_scrap.Id_potensi INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE concat(tb_potensi.Sto,tb_potensi.Lokasi,tb_order_scrap.Id_order) LIKE %s and tb_sto.Witel=%s",("%"+value+"%",witel))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_update'] = str(datas[x]['Dt_update'])
    dataJson = json.dumps(datas)
    return dataJson

def search_get_all_hasil_scrap(value,Id_order):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_scrap`, `Id_order`, `Jenis_barang`, `Dt_scrap`, `Nama_barang`, `Satuan`, `Jumlah`, `Sts_check` FROM `tb_hasil_scrap` WHERE concat(`Id_scrap`,`Jenis_barang`, `Nama_barang`,`Jumlah`) LIKE %s and `Id_order`= %s",("%"+value+"%",Id_order))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    
    except(mysql.connector.Error,mysql.connector.Warning) as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)

    for x in range(0,len(datas)):
        datas[x]['Dt_scrap'] = str(datas[x]['Dt_scrap'])
        datas[x]['Jumlah'] = str(datas[x]['Jumlah'])
    dataJson = json.dumps(datas)
    return dataJson

def search_get_potensi_all(value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT `Id_potensi`, `Sto`, `Jenis_barang`, `Lokasi`, `Nama_barang`,`Satuan`, `Jumlah`, `Lat`, `Lng`,`Status` FROM `tb_potensi` WHERE concat(`Id_potensi`, `Sto`, `Jenis_barang`, `Lokasi`, `Nama_barang`) LIKE %s",("%"+value+"%",))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson

def search_get_potensi_base_on_regional(Regional,value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Id_potensi,tb_potensi.Sto,tb_potensi.Jenis_barang,tb_potensi.Lokasi,tb_potensi.Nama_barang,tb_potensi.Satuan,tb_potensi.Jumlah,tb_potensi.Lat,tb_potensi.Lng,tb_potensi.Status FROM `tb_potensi` INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE concat(tb_potensi.Id_potensi,tb_potensi.Sto,tb_potensi.Jenis_barang,tb_potensi.Lokasi,tb_potensi.Nama_barang) LIKE %s and tb_sto.Regional = %s",("%"+value+"%",Regional))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson

def search_get_potensi_base_on_witel(Witel,value):
    db = sql_connection()
    cursor = db.cursor()
    try:
        cursor.execute("SELECT tb_potensi.Id_potensi,tb_potensi.Sto,tb_potensi.Jenis_barang,tb_potensi.Lokasi,tb_potensi.Nama_barang,tb_potensi.Satuan,tb_potensi.Jumlah,tb_potensi.Lat,tb_potensi.Lng,tb_potensi.Status FROM `tb_potensi` INNER JOIN tb_sto ON tb_potensi.Sto = tb_sto.Sto WHERE concat(tb_potensi.Id_potensi,tb_potensi.Sto,tb_potensi.Jenis_barang,tb_potensi.Lokasi,tb_potensi.Nama_barang) LIKE  %s AND tb_sto.Witel = %s",("%"+value+"%",Witel))
        rows = [x for x in cursor]
        cols = [x[0] for x in cursor.description]
    except(mysql.connector.Error,mysql.connector.Warning)as e:
        print(e)
        rows = []
        cols = []
    datas = []
    for row in rows:
        data = {}
        for prop, val in zip(cols, row):
            data[prop] = val
        datas.append(data)
        
    for x in range(0,len(datas)):
        datas[x]['Lat'] = str(datas[x]['Lat'])
        datas[x]['Lng'] = str(datas[x]['Lng'])
    dataJson = json.dumps(datas)
    return dataJson
