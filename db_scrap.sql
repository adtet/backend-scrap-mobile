create database db_scrap; 
/*create user 'scrap321'@'localhost' identified by 'scrap123456';grant all privileges on db_scrap.* to 'scrap'@'localhost';

drop table tb_menu;*/ 
CREATE TABLE `tb_menu` ( 
  `Id_menu` varchar(6) NOT NULL, 
  `Group_menu` varchar(20) DEFAULT NULL, 
  `Nama_menu` varchar(20) NOT NULL DEFAULT '-', 
  `Icon` varchar(32) NOT NULL DEFAULT 'fa fa-circle-o', 
  `Parent` varchar(6) NOT NULL DEFAULT '', 
  `Pos_menu` int(11) DEFAULT '1', 
  `Otoritas` varchar(12) DEFAULT NULL, 
  `Link_menu` varchar(64) DEFAULT NULL, 
  PRIMARY KEY (`Id_menu`), 
  KEY `Nama_menu` (`Nama_menu`), 
  KEY `Parent` (`Parent`), 
  KEY `Otoritas` (`Otoritas`) 
); 
delete from tb_menu;
insert into tb_menu values ('01','Admin Menu','Admin','fa fa-expeditedssl','',1,'0,1','#');
insert into tb_menu values ('0101','Admin Menu','User-admin','fa fa-users','01',1,'0,1','admin_user');
insert into tb_menu values ('0102','Admin Menu','-','','01',2,'0','#');
insert into tb_menu values ('0103','Admin Menu','Menu','fa fa-list','01',3,'0','admin_menu'); 
insert into tb_menu values ('0104','Admin Menu','-','','01',4,'0','#'); 
insert into tb_menu values ('0105','Admin Menu','Otoritas','fa fa-shield','01',5,'0','admin_oto'); 
insert into tb_menu values ('02','Main Menu','Master','fa fa-cubes','',2,'0,1,2','#'); 
insert into tb_menu values ('0201','Main Menu','STO','fa fa-building','02',2,'0,1,2','master_sto'); 
insert into tb_menu values ('03','Main Menu','Dashboard','fa fa-dashboard','',3,'0,1,2,3,4','dashboard'); 
insert into tb_menu values ('04','Main Menu','Potensi','fa fa-list','',4,'0,1,2,3,4','#'); 
insert into tb_menu values ('0401','Main Menu','Data potensi','fa fa-list','04',1,'0,1,2,3,4','potensi'); 
insert into tb_menu values ('0402','Main Menu','-','','04',2,'0','#');
insert into tb_menu values ('0403','Main Menu','Input potensi','fa fa-plus','04',3,'0,1,2,3','potensi/add'); 
insert into tb_menu values ('05','Main Menu','Report','fa fa-file-text','',5,'0,1,2,3','#'); 
insert into tb_menu values ('0501','Main Menu','Rekap data potensi','fa fa-file-text-o','05',1,'0,1,2,3,4','report/potensi'); 
insert into tb_menu values ('0502','Main Menu','-','','05',2,'1','#');
insert into tb_menu values ('0503','Main Menu','Rekap Scrap','fa fa-file-text','05',1,'0,1,2,3,4','report/scrap'); 


/* drop table tb_otoritas; */
/*drop table tb_otoritas;*/
create table tb_otoritas (
    Oto_id int not null,
    Oto_name varchar(16),
    primary key (Oto_id)
);
insert into tb_otoritas (Oto_id, Oto_name) values (0,'ADMIN'),(1,'SPUSER'),(2,'MASTER'),(3,'WASPANG'),(4,'USER')

/* drop table tb_user; */
/*drop table tb_user;*/
create table tb_user (
    User_id varchar(12),
    Nama varchar(32),
    Perusahaan varchar(12),
    Regional integer default 0,
    Witel varchar(12),
    Otoritas int default 3, /* Otoritas (0:admin;1:superuser;2:waspang;3:user) */
    Photo varchar(64),
    Sandi varchar(256), 
    Status_user varchar(12), /* Status_user (UNCHECK;ACTIVE;REJECT) */
    Note text, 
    Telegram_id varchar(32) DEFAULT NULL, 
    Otp varchar(32) DEFAULT NULL,
    Dt_expired datetime DEFAULT NULL,
    Dt_add datetime, 
    Dt_update datetime, 
    Dt_lastlogin datetime, 
    Dt_chgpass datetime, 
    Dt_expired datetime, 
    primary key (User_id) 
);
insert into tb_user (User_id, Nama, Perusahaan, Regional, Witel, Otoritas, Photo, Sandi, Status_user, Dt_add, Dt_update) 
values ('admin', 'ADMINISTRATOR', 'TELKOM', 3, 'ALL', '0', 'admin.png', md5('Admin123'), 'ACTIVE', now(), null);

/* drop table tb_sto; */
CREATE TABLE tb_sto (
    Sto varchar(5) NOT NULL,
    Nama_sto varchar(32) DEFAULT NULL,
    Regional int(11) DEFAULT NULL,
    Witel varchar(32) DEFAULT NULL,
    Datel varchar(32) DEFAULT NULL,
    Ubis varchar(32) DEFAULT NULL,
    Lat decimal(13,10) DEFAULT NULL,
    Lng decimal(13,10) DEFAULT NULL,
    PRIMARY KEY (Sto)
);
insert into tb_sto values 
('AWN','ARJAWINANGUN','3','CIREBON','INNERCBN','ARJAWINANGUN','-6.6479290000','108.4071650000'),
('BDK','BANDUNG KOPO','3','BANDUNG','HS-2','KOPO','-6.9710740000','107.5736150000'),
('BGL','BOJONG LOPANG','3','SUKABUMI','SUKABUMI','PELABUHANRATU','-7.0571542030','106.8009323349'),
('BJA','BANJARAN','3','BANDUNGBRT','SOREANG','BANJARAN','-7.0328600000','107.5941370000'),
('BJS','BANJARSARI','3','TASIKMALAYA','BANJAR','BANJAR','-7.4817429390','108.6046979871'),
('BNJ','BANJAR','3','TASIKMALAYA','BANJAR','BANJAR','-7.3699943340','108.5409410288'),
('BON','BALONGAN','3','CIREBON','INDRAMAYU','INDRAMAYU','-6.3740890000','108.3924820000'),
('BTJ','BATUJAJAR','3','BANDUNGBRT','PADALARANG','BATUJAJAR','-6.9045210000','107.5026960000'),
('CAS','CIASEM','3','KARAWANG','SUBANG','PAMANUKAN','-6.3385798699','107.6656536123'),
('CBD','CIBADAK','3','SUKABUMI','CIBADAK','CIBADAK','-6.8889835396','106.7802347868'),
('CBE','CIBEBER','3','SUKABUMI','CIANJUR','CIRANJANG','-6.9358448877','107.1218934650'),
('CBL','CIBALONG','3','TASIKMALAYA','SINGAPARNA','SINGAPARNA','0.0000000000','0.0000000000'),
('CBN','CIREBON','3','CIREBON','INNERCBN','CIREBON','-6.7160100000','108.5610930000'),
('CBT','CIBATU','3','TASIKMALAYA','GARUT','WANARAJA','0.0000000000','0.0000000000'),
('CBU','CIBUNGUR','3','KARAWANG','PURWAKARTA','PURWAKARTA','0.0000000000','0.0000000000'),
('CCD','CICADAS','3','BANDUNG','HS-1','CICADAS','-6.9055430000','107.6495980000'),
('CCL','CICALENGKA','3','BANDUNGBRT','SOREANG','RANCAEKEK','-6.9808040000','107.4389600000'),
('CCR','CICURUG','3','SUKABUMI','CIBADAK','CICURUG','0.0000000000','0.0000000000'),
('CIW','CIAWI','3','TASIKMALAYA','SINGAPARNA','SINGAPARNA','0.0000000000','0.0000000000'),
('CJA','CIJAURA','3','BANDUNG','SUMEDANG','CIJAWURA','-6.9599200000','107.6625980000'),
('CJG','CIRANJANG','3','SUKABUMI','CIANJUR','CIRANJANG','-6.8128830000','107.2498830000'),
('CJR','CIANJUR','3','SUKABUMI','CIANJUR','CIANJUR','-6.8177530000','107.1366790000'),
('CKB','CIKEMBANG','3','SUKABUMI','CIBADAK','CIBADAK','0.0000000000','0.0000000000'),
('CKC','CIREBON KANCI','3','CIREBON','INNERCBN','CIREBON','0.0000000000','0.0000000000'),
('CKI','CIKIJING','3','CIREBON','MAJALENGKA','MAJALENGKA','0.0000000000','0.0000000000'),
('CKJ','CIKAJANG','3','TASIKMALAYA','GARUT','GARUT','0.0000000000','0.0000000000'),
('CKK','CIKALONG KULON','3','SUKABUMI','SINDANGLAYA','SINDANGLAYA','0.0000000000','0.0000000000'),
('CKP','CIKAMPEK','3','KARAWANG','PURWAKARTA','CIKAMPEK','0.0000000000','0.0000000000'),
('CKW','CIKALONGWETAN','3','BANDUNGBRT','PADALARANG','PADALARANG','-6.7349070000','107.4379310000'),
('CKY','CIREBON KARYAMULYA','3','CIREBON','INNERCBN','CIREBON','0.0000000000','0.0000000000'),
('CLI','CILIMUS','3','CIREBON','KUNINGAN','KUNINGAN','0.0000000000','0.0000000000'),
('CLL','CILILIN','3','BANDUNGBRT','PADALARANG','BATUJAJAR','-6.9808390000','107.4389880000'),
('CLM','CILAMAYA','3','KARAWANG','PURWAKARTA','CIKAMPEK','0.0000000000','0.0000000000'),
('CMI','CIMAHI','3','BANDUNGBRT','PADALARANG','CIMAHI','-6.8780660000','107.5490850000'),
('CMO','CIMANGKOK','3','SUKABUMI','INNERSKB','SUKABUMI','0.0000000000','0.0000000000'),
('CMS','CIAMIS','3','TASIKMALAYA','CIAMIS','CIAMIS','0.0000000000','0.0000000000'),
('CPL','CIAMPEL','3','KARAWANG','INNERKWA','KLARI','0.0000000000','0.0000000000'),
('CPT','CIPATAT','3','BANDUNGBRT','PADALARANG','PADALARANG','-6.8279300000','107.3770080000'),
('CSA','CISARUA LEMBANG','3','BANDUNGBRT','PADALARANG','LEMBANG','-6.8158740000','107.5574950000'),
('CSP','CISOMPET','3','TASIKMALAYA','GARUT','GARUT','0.0000000000','0.0000000000'),
('CWD','CIWIDEY','3','BANDUNGBRT','SOREANG','SOREANG','-7.0509590000','107.7535440000'),
('DGO','DAGO','3','BANDUNG','HS-1','DAGO','-6.8895320000','107.6261940000'),
('GGK','GEGERKALONG','3','BANDUNG','HS-1','GEGERKALONG','-6.8698210000','107.5893240000'),
('GNH','GUNUNGHALU','3','BANDUNGBRT','PADALARANG','BATUJAJAR','-7.0229990000','107.3128150000'),
('GRU','GARUT','3','TASIKMALAYA','GARUT','GARUT','0.0000000000','0.0000000000'),
('HAR','HAURGEULIS','3','CIREBON','INDRAMAYU','LOSARANG','0.0000000000','0.0000000000'),
('HGM','HEGARMANAH','3','BANDUNG','HS-1','HEGARMANAH','-6.8827040000','107.6019450000'),
('IMY','INDRAMAYU','3','CIREBON','INDRAMAYU','INDRAMAYU','-6.3254660000','108.3239440000'),
('JBN','JAMBLANG','3','CIREBON','INNERCBN','ARJAWINANGUN','-6.7098740000','108.4444760000'),
('JCG','JALANCAGAK','3','KARAWANG','SUBANG','SUBANG','0.0000000000','0.0000000000'),
('JPK','JAMPANG KULON','3','SUKABUMI','CIBADAK','PELABUHANRATU','0.0000000000','0.0000000000'),
('JTB','JATIBARANG','3','CIREBON','INDRAMAYU','INDRAMAYU','0.0000000000','0.0000000000'),
('JTS','JATISARI','3','KARAWANG','PURWAKARTA','CIKAMPEK','0.0000000000','0.0000000000'),
('JTW','JATIWANGI','3','CIREBON','MAJALENGKA','JATIWANGI','0.0000000000','0.0000000000'),
('KAD','KADIPATEN','3','CIREBON','MAJALENGKA','JATIWANGI','0.0000000000','0.0000000000'),
('KAW','KAWALI','3','TASIKMALAYA','CIAMIS','CIAMIS','0.0000000000','0.0000000000'),
('KDN','KADUNGORA','3','TASIKMALAYA','GARUT','WANARAJA','0.0000000000','0.0000000000'),
('KIA','KALIJATI','3','KARAWANG','SUBANG','PAMANUKAN','0.0000000000','0.0000000000'),
('KLI','KLARI','3','KARAWANG','INNERKWA','KLARI','0.0000000000','0.0000000000'),
('KLU','KELAPANUNGGAL','3','SUKABUMI','CIBADAK','CICURUG','0.0000000000','0.0000000000'),
('KNG','KUNINGAN','3','CIREBON','KUNINGAN','KUNINGAN','-6.9811910000','108.4788460000'),
('KNU','KARANG NUNGGAL','3','TASIKMALAYA','SINGAPARNA','SINGAPARNA','0.0000000000','0.0000000000'),
('KRL','KARANGLIGAR','3','KARAWANG','INNERKWA','KARAWANG','0.0000000000','0.0000000000'),
('KRM','KARANGAMPEL','3','CIREBON','INDRAMAYU','INDRAMAYU','0.0000000000','0.0000000000'),
('KRW','KARAWANG','3','KARAWANG','INNERKWA','KARAWANG','0.0000000000','0.0000000000'),
('LAG','LIMBANGAN','3','TASIKMALAYA','GARUT','WANARAJA','0.0000000000','0.0000000000'),
('LBG','LEMBONG','3','BANDUNG','HS-2','LEMBONG','-6.9168090000','107.6110960000'),
('LEM','LEMBANG','3','BANDUNGBRT','PADALARANG','LEMBANG','-6.8116240000','107.6167780000'),
('LOS','LOSARI','3','CIREBON','KUNINGAN','LOSARI','0.0000000000','0.0000000000'),
('LSR','LOSARANG','3','CIREBON','INDRAMAYU','LOSARANG','0.0000000000','0.0000000000'),
('MJL','MAJALENGKA','3','CIREBON','MAJALENGKA','MAJALENGKA','0.0000000000','0.0000000000'),
('MJY','MAJALAYA','3','BANDUNGBRT','SOREANG','MAJALAYA','-7.0508550000','107.7529750000'),
('MLB','MALANGBONG','3','TASIKMALAYA','GARUT','WANARAJA','0.0000000000','0.0000000000'),
('MNJ','MANONJAYA','3','TASIKMALAYA','CIAMIS','CIAMIS','0.0000000000','0.0000000000'),
('NJG','NANJUNG','3','BANDUNGBRT','PADALARANG','CIMAHI','-6.9215270000','107.5422400000'),
('NLD','NYALINDUNG','3','SUKABUMI','INNERSKB','SUKABUMI','0.0000000000','0.0000000000'),
('PAB','PABUARAN CIREBON','3','CIREBON','KUNINGAN','LOSARI','0.0000000000','0.0000000000'),
('PAX','PANGANDARAN','3','TASIKMALAYA','BANJAR','BANJAR','0.0000000000','0.0000000000'),
('PBS','PABUARAN SUBANG','3','KARAWANG','SUBANG','PAMANUKAN','0.0000000000','0.0000000000'),
('PCH','PADALARANG2','3','BANDUNGBRT','PADALARANG','PADALARANG','0.0000000000','0.0000000000'),
('PDL','PADALARANG','3','BANDUNGBRT','PADALARANG','PADALARANG','-6.8364560000','107.4891850000'),
('PGD','PAGADEN','3','KARAWANG','SUBANG','PAMANUKAN','0.0000000000','0.0000000000'),
('PLD','PLERED KARAWANG','3','KARAWANG','PURWAKARTA','PURWAKARTA','0.0000000000','0.0000000000'),
('PLR','PELABUHANRATU','3','SUKABUMI','CIBADAK','PELABUHANRATU','0.0000000000','0.0000000000'),
('PMN','PAMANUKAN','3','KARAWANG','SUBANG','PAMANUKAN','0.0000000000','0.0000000000'),
('PMP','PAMEMPEUK','3','TASIKMALAYA','GARUT','GARUT','0.0000000000','0.0000000000'),
('PNL','PANGALENGAN','3','BANDUNGBRT','SOREANG','BANJARAN','0.0000000000','0.0000000000'),
('PRD','PLERED','3','CIREBON','INNERCBN','ARJAWINANGUN','-6.7105590000','108.5047060000'),
('PTR','PATROL','3','CIREBON','INDRAMAYU','LOSARANG','0.0000000000','0.0000000000'),
('PWK','PURWAKARTA','3','KARAWANG','PURWAKARTA','PURWAKARTA','0.0000000000','0.0000000000'),
('RCK','RANCAEKEK','3','BANDUNGBRT','SOREANG','RANCAEKEK','-6.9544040000','107.7690380000'),
('RDK','RENGASDENGKLOK','3','KARAWANG','INNERKWA','KARAWANG','0.0000000000','0.0000000000'),
('RGA','RAJAGALUH','3','CIREBON','MAJALENGKA','JATIWANGI','0.0000000000','0.0000000000'),
('RJP','RAJAPOLAH','3','TASIKMALAYA','INNERTSM','TASIKMALAYA','0.0000000000','0.0000000000'),
('RJW','BANDUNG RAJAWALI','3','BANDUNGBRT','RAJAWALI','RAJAWALI','-6.9127890000','107.5753060000'),
('SDL','SINDANGLAYA','3','SUKABUMI','SINDANGLAYA','SINDANGLAYA','0.0000000000','0.0000000000'),
('SDU','SINDANGLAUT','3','CIREBON','KUNINGAN','LOSARI','0.0000000000','0.0000000000'),
('SGA','SUKANEGARA','3','SUKABUMI','CIANJUR','CIRANJANG','0.0000000000','0.0000000000'),
('SGN','SAGARANTEN','3','SUKABUMI','INNERSKB','SUKABUMI','0.0000000000','0.0000000000'),
('SKB','SUKABUMI','3','SUKABUMI','INNERSKB','SUKABUMI','-6.9204740000','106.9287310000'),
('SKM','SUKARESMI','3','SUKABUMI','SINDANGLAYA','SINDANGLAYA','0.0000000000','0.0000000000'),
('SMD','SUMEDANG','3','BANDUNG','SUMEDANG','SUMEDANG','-6.8591980000','107.9219600000'),
('SOR','SOREANG','3','BANDUNGBRT','SOREANG','SOREANG','-7.0212460000','107.5250140000'),
('SPA','SINGAPARNA','3','TASIKMALAYA','SINGAPARNA','SINGAPARNA','0.0000000000','0.0000000000'),
('SUB','SUBANG','3','KARAWANG','SUBANG','SUBANG','0.0000000000','0.0000000000'),
('TAS','TANJUNGSARI','3','BANDUNG','SUMEDANG','SUMEDANG','-6.9029940000','107.8012000000'),
('TGE','TANGGEUNG','3','SUKABUMI','CIANJUR','CIRANJANG','0.0000000000','0.0000000000'),
('TLE','TEGALEGA','3','BANDUNG','HS-2','TEGALEGA','-6.9475470000','107.6090250000'),
('TLJ','TELUKJAMBE','3','KARAWANG','INNERKWA','KLARI','0.0000000000','0.0000000000'),
('TRG','TURANGGA','3','BANDUNG','HS-2','TURANGGA','-6.9344160000','107.6270360000'),
('TSM','TASIKMALAYA','3','TASIKMALAYA','INNERTSM','TASIKMALAYA','0.0000000000','0.0000000000'),
('UBR','UJUNGBERUNG','3','BANDUNG','SUMEDANG','UJUNGBERUNG','-6.9217310000','107.7095370000'),
('WDS','WADAS','3','KARAWANG','INNERKWA','KLARI','0.0000000000','0.0000000000'),
('WNR','WANARAJA','3','TASIKMALAYA','GARUT','WANARAJA','0.0000000000','0.0000000000');

/*drop table tb_perusahaan;*/
create table tb_perusahaan (
    Id_perusahaan varchar(16),
    Nama_perusahaan varchar(32),
    Sortid int default 2,
    primary key (Id_perusahaan)
);
insert into tb_perusahaan (Id_perusahaan, Nama_perusahaan, Sortid) values 
('TELKOM','PT TELKOM INDONESIA TBK',1),('TA','PT TELKOM AKSES',2),('DEVINA','DEVINA-SUBCON TA',3);

/*drop table tb_status_order;*/
create table tb_status_order (
    Id_status int,
    Sts_order varchar(20),
    primary key (Id_status)
);
insert into tb_status_order (Id_status, Sts_order) values 
(0,'POTENSI'),(1,'ORDER'),(2,'PLAN SURVEY'),(3,'SURVEY'),(4,'PERIJINAN'),(5,'SCRAP'),(6,'SELESAI')

/*drop table tb_satuan_potensi;*/
create table tb_satuan_potensi (
    Satuan varchar(20),
    Sortid int default 1,
    primary key (Satuan)
);
insert into tb_satuan_potensi (Satuan, Sortid) values 
('KG',1),('MTR',2),('RAK',3),('LEMBAR',4),('BH',5)

DROP TABLE IF EXISTS `tb_jenis_potensi`;
CREATE TABLE `tb_jenis_potensi` ( 
    `Id_jenispotensi` varchar(16) NOT NULL, 
    `Nama_jenispotensi` varchar(32) DEFAULT NULL, 
    `Sortid` int(11) DEFAULT NULL, 
    PRIMARY KEY (`Id_jenispotensi`) 
); 
insert into tb_jenis_potensi (Id_jenispotensi, Nama_jenispotensi, Sortid) values 
('KT_PRIMER','KABEL TEMBAGA, PRIMER',1), 
('KT_SEKUNDER','KABEL TEMBAGA, SEKUNDER',2), 
('KTTL','KABEL TEMBAGA, TANAM LANGSUNG',3),
('DSLAM','DSLAM',4), 
('MSAN','MSAN',5), 
('BATERY','BATERY',6), 
('SENTRAL','SENTRAL',7), 
('TOWER','TOWER',8);

DROP TABLE IF EXISTS `tb_potensi`;
CREATE TABLE `tb_potensi` ( 
  `Id_potensi` varchar(20) NOT NULL, 
  `Sto` varchar(3) DEFAULT NULL,
  `Jenis_barang` varchar(32) DEFAULT NULL, 
  `Lokasi` varchar(128) DEFAULT NULL,
  `Nama_barang` varchar(32) DEFAULT NULL, 
  `Vendor` varchar(32) DEFAULT NULL,
  `Kategori` varchar(20) DEFAULT NULL, 
  `Satuan` varchar(20) DEFAULT NULL, 
  `Jumlah` int(11) DEFAULT NULL, 
  `Note` text, 
  `Foto` varchar(128) DEFAULT NULL, 
  `Lat` decimal(10,7) DEFAULT NULL, 
  `Lng` decimal(10,7) DEFAULT NULL, 
  `Status` int(11) DEFAULT '0', /*0.POTENSI;1.ORDER;2.PLAN_SURVEY;3.SURVEY;4.PERIJINAN;5.SCRAP;6.SELESAI*/ 
  `Jumlah_sisa` int(11) DEFAULT NULL, 
  `Dt_add` datetime DEFAULT NULL, 
  `Add_by` varchar(20) DEFAULT NULL, 
  `Dt_update` datetime DEFAULT NULL, 
  `Update_by` varchar(20) DEFAULT NULL, 
  PRIMARY KEY (`Id_potensi`), 
  KEY `Sto` (`Sto`) 
);

DROP TABLE IF EXISTS tb_order_scrap;
CREATE TABLE tb_order_scrap ( 
    Id_order varchar(20) NOT NULL,
    Id_potensi varchar(20) NOT NULL,
    Mitra varchar(16), 
    Dt_order datetime,
    Dt_target datetime,
    Panjang int, 
    Jumlah int, 
    Satuan2 varchar(16), 
    Jumlah2 int default 0, 
    Note varchar(16), 
    Waspang_telkom varchar(32),
    Waspang_mitra varchar(32),
    Mitra_pelaksana varchar(32),
    Status_order int, 
    Dt_add datetime,
    Update_by varchar(12), 
    Dt_update datetime,
    Sts_approve int, 
    Approve_by varchar(12),
    Dt_approve datetime,
    primary key (Id_order),
    key (Id_potensi),
    key (Mitra),
    key (Status_order)
);

DROP TABLE IF EXISTS tb_order_scrap_his; 
CREATE TABLE tb_order_scrap_his ( 
    Id_order varchar(20) NOT NULL,
    Id_potensi varchar(20) NOT NULL,
    Mitra varchar(16), 
    Dt_order datetime,
    Dt_target datetime,
    Panjang int, 
    Jumlah int, 
    Satuan2 varchar(16), 
    Jumlah2 varchar(16), 
    Note varchar(16), 
    Status_order int, 
    Update_by varchar(12), 
    Dt_add datetime,
    Dt_update datetime,
    Sts_approve int, 
    Approve_by varchar(12),
    Dt_approve datetime,
    primary key (Id_order),
    key (Id_potensi),
    key (Mitra),
    key (Status_order)
);

DROP TABLE IF EXISTS tb_hasil_scrap;
CREATE TABLE tb_hasil_scrap ( 
    Id_order varchar(20) NOT NULL,
	Id_potensi varchar(20) NOT NULL,
    Jenis_barang varchar(20),
    Dt_scrap date,
    Nama_barang varchar(32),
    From_ varchar(32),
    To_ varchar(32),
    Pair_ varchar(10),
    Diameter_ varchar(10),
    Potongan_4M int,
    Potongan_sisa int,
    Satuan varchar(20),
    Jumlah int,
    Note text,
    Dt_add datetime, 
    Update_by varchar(12), 
    Dt_update datetime,
    Sts_check int, 
    Check_by varchar(12),
    Dt_check datetime,
    primary key (Id_order),
    key (Id_potensi)
);


